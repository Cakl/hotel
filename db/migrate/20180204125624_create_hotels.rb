class CreateHotels < ActiveRecord::Migration
  def change
    create_table :hotels do |t|
      t.string :name
      t.string :adress
      t.integer :price
      t.text :description

      t.timestamps null: false
    end
  end
end
