class CreateCommentaries < ActiveRecord::Migration
  def change
    create_table :commentaries do |t|
      t.integer :score
      t.text :comment

      t.timestamps null: false
    end
  end
end
