class AddHotelIdToCommentaries < ActiveRecord::Migration
  def change
    add_column :commentaries, :hotel_id, :integer
  end
end
