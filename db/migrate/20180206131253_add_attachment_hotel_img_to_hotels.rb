class AddAttachmentHotelImgToHotels < ActiveRecord::Migration
  def self.up
    change_table :hotels do |t|
      t.attachment :hotel_img
    end
  end

  def self.down
    remove_attachment :hotels, :hotel_img
  end
end
