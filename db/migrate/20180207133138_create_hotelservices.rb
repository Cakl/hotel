class CreateHotelservices < ActiveRecord::Migration
  def change
    create_table :hotelservices do |t|
      t.integer :hotel_id, :null => false
      t.integer :service_id, :null => false

      t.timestamps null: false
    end
  end
end
