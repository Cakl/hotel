class Destination < ActiveRecord::Base
  has_many :hotels
  has_many :cities

  validates_associated :cities
end
