$city_id = 0

class CityValidator < ActiveModel::Validator
  def validate(record)
    $city_id = City.find(record.send(:city_id)).destination_id
  end
end

class DestValidator < ActiveModel::Validator
  def validate(record)
    if Destination.find(record.send(:destination_id)).id != $city_id
      record.errors[:base] << "Error"
    end
  end
end


class Hotel < ActiveRecord::Base
  belongs_to :user
  belongs_to :destination
  belongs_to :city
  has_many :commentaries

  has_many :hotelservices
  has_many :services, :through => :hotelservices
  has_many :hotellandmarks
  has_many :landmarks, :through => :hotellandmarks

  validates :name, :presence => true
  validates :city_id, :presence => true



  validates_with CityValidator, fields: [:city_id]
  validates_with DestValidator, fields: [:destination_id]



  has_attached_file :hotel_img, styles: { :hotel_index => "150x250>", :hotel_show => "250x350" }, default_url: "/images/:style/missing.png"
  validates_attachment_content_type :hotel_img, content_type: /\Aimage\/.*\z/


end
