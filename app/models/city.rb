class City < ActiveRecord::Base
  belongs_to :destination

  validates_associated :destination
end
