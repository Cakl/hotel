class Commentary < ActiveRecord::Base
  belongs_to :hotel
  belongs_to :user

  validates :comment, :presence => true
  validates :score, :presence => true
end
