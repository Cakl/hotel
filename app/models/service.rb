class Service < ActiveRecord::Base

  has_many :hotelservices
  has_many :hotels, :through => :hotelservices
end
