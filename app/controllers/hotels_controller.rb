class HotelsController < ApplicationController
  before_action :find_hotel, only:[:show,:edit, :update, :destroy]
  before_action :authenticate_user!, only: [:new,:edit]

  def index
    @hotel_ids=[]
    if params[:destination].blank?
      if params[:city].blank?
        if params[:service].blank?
          @hotels = Hotel.all.order("created_at DESC")
        else
          @service_id = Service.find_by(name: params[:service]).id
          @hotel_ids = Hotelservice.where(:service_id => @service_id).order("created_at DESC")
          @hotels = Hotel.all.order("created_at DESC")
        end
      else
        if params[:service].blank?
          @city_id = City.find_by(name: params[:city]).id
          @hotels = Hotel.where(:city_id => @city_id).order("created_at DESC")
        else
          @service_id = Service.find_by(name: params[:service]).id
          @hotel_ids = Hotelservice.where(:service_id => @service_id).order("created_at DESC")
          @hotels = Hotel.where(:city_id => @city_id).order("created_at DESC")
        end
      end
    else
      if params[:city].blank?
        if params[:service].blank?
          @destination_id = Destination.find_by(name: params[:destination]).id
          @hotels = Hotel.where(:destination_id => @destination_id).order("created_at DESC")
        else
          @destination_id = Destination.find_by(name: params[:destination]).id
          @service_id = Service.find_by(name: params[:service]).id
          @hotel_ids = Hotelservice.where(:service_id => @service_id).order("created_at DESC")
          @hotels = Hotel.where(:destination_id => @destination_id).order("created_at DESC")
        end
      else
        if params[:service].blank?
          @destination_id = Destination.find_by(name: params[:destination]).id
          @city_id = City.find_by(name: params[:city]).id
          @hotels = Hotel.where(:destination_id => @destination_id,:city_id => @city_id).order("created_at DESC")
        else
          @destination_id = Destination.find_by(name: params[:destination]).id
          @city_id = City.find_by(name: params[:city]).id
          @service_id = Service.find_by(name: params[:service]).id
          @hotel_ids = Hotelservice.where(:service_id => @service_id).order("created_at DESC")
          @hotels = Hotel.where(:destination_id => @destination_id,:city_id => @city_id).order("created_at DESC")
        end
      end
    end
  end

  def show
    if @hotel.commentaries.blank?
      @average_comment = 0
    else
      @average_comment = @hotel.commentaries.average(:score).round(2)
    end
  end

  def new
    @hotel = current_user.hotels.build
    @destinations = Destination.all.map{ |d| [d.name,d.id] }
    @cities = City.all.map{ |c| [c.name,c.id] }


    @all_services = Service.all
    @hotel_service = @hotel.hotelservices.build

    @all_landmarks = Landmark.all
    @hotel_landmark = @hotel.hotellandmarks.build


  end

  def create
    @hotel = current_user.hotels.build(hotel_params)
    @hotel.destination_id=params[:destination_id]
    @hotel.city_id=params[:city_id]

    params[:services][:id].each do |service|
      if !service.empty?
        @hotel.hotelservices.build(:service_id => service)
      end
    end

    params[:landmarks][:id].each do |landmark|
      if !landmark.empty?
        @hotel.hotellandmarks.build(:landmark_id => landmark)
      end
    end


    respond_to do |format|
      if @hotel.save
        format.html { redirect_to @hotel, notice: 'Hotel was successfully created.' }
        format.json { render :show, status: :created, location: @hotel }
      else
        format.html { redirect_to new_hotel_path, notice: 'Hotel was not successfully created.' }
        format.json { render json: @hotel.errors, status: :unprocessable_entity }
      end
    end




  end

  def edit
    @destinations = Destination.all.map{ |d| [d.name,d.id] }
    @cities = City.all.map{ |c| [c.name,c.id] }

  end

  def update
    @hotel.destination_id=params[:destination_id]
    @hotel.city_id=params[:city_id]

    if @hotel.update(hotel_params)
      redirect_to hotel_path(@hotel)
    else
      render 'edit'
    end
  end

  def destroy
  @hotel.destroy
    redirect_to root_path
  end

  private

    def hotel_params
      params.require(:hotel).permit(:name,:adress,:price,:description,:destination_id,:city_id,:hotel_img)
    end

    def find_hotel
      @hotel = Hotel.find(params[:id])
    end

end
