json.extract! landmark, :id, :name, :created_at, :updated_at
json.url landmark_url(landmark, format: :json)
